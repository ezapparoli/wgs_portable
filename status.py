#!/usr/bin/env python
import subprocess
import sys

jobid = sys.argv[1]

output = str(subprocess.check_output("qstat -x -f %s | grep job_state | cut -f 2 -d '=' | xargs" % jobid, shell=True).strip())

running_status=["B", "H", "Q", "R", "S"]
if "F" in output:
  print("success")
elif any(r in output for r in running_status):
  print("running")
else:
  print("failed")
