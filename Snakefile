#mkdir -p all_samples
#for myfastq in $(ls ../Sample_*/*.fastq.gz); do ln -s $myfastq . ; done

#snakemake --config RUN=190412_A00626_0030_AHJMFTDSXX PRJ=PapponeC_955_Brugada_WGS --cluster "qsub -o errors -e errors -N smk.{name}.{jobid}.sh" --jobs 20 --use-conda chunks

#snakemake --config RUN=190412_A00626_0030_AHJMFTDSXX PRJ=PapponeC_955_Brugada_WGS --cluster "qsub -o errors -e errors -N smk.{name}.{jobid}.sh" --jobs 20 --use-conda

configfile: "config.yaml"

#workdir: outdir
#cercare in raw data con il nome della run (Jun_Jul issue)
#proc = subprocess.Popen(["find /lustre1/SampleSheets/ -maxdepth 2 -type d -name " + config['RUN']] + "_IEM.csv", stdout=subprocess.PIPE, shell=True)
#(out, err) = proc.communicate()
#SAMPLESHEET = str(out.strip().decode("utf-8")
#RAW_DIR = str(out.strip().decode("utf-8") + "/Data/Intensities/BaseCalls")
RAW_DIR = "/lustre2/raw_data/" + config['RUN'] + "/Project_" + config['PRJ'] + "/all_samples"
#RAW_DIR = "/illumina/May_Jun/" + config['RUN'] + "/Data/Intensities"
SAMPLES_TMP, = glob_wildcards(RAW_DIR+"/{sample}_R1_001.fastq.gz")
SAMPLES = [x for x in SAMPLES_TMP if not x.startswith('Undetermined')]
#NAMES = [x.split('_')[0] for x in SAMPLES]
NAMES = [x for x in SAMPLES]
#IDS = [x.split('_')[1] for x in SAMPLES]
#LANES = [x.split('_')[2] for x in SAMPLES]
#MYSAMPLES_TMP, = glob_wildcards(config['RUN']+"/TRIMMED/{mysample}_L001_R1.duk.fq.gz")
#MYSAMPLES_TMP, = glob_wildcards(RAW_DIR+"/{mysample}_L001_R1_001.fastq.gz")
MYSAMPLES_TMP, = glob_wildcards(config['RUN']+"/BAM/{mysample}_L001.bam")
MYSAMPLES = [x for x in MYSAMPLES_TMP]
MYSAMPLES_BAM_TMP, = glob_wildcards(config['RUN']+"/BAM/{mysample}_markdup.bam")
MYSAMPLES_BAM = [x for x in MYSAMPLES_BAM_TMP]
#MYCHR = ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"]
MYCHR = ["chr1", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr2", "chr20", "chr21", "chr22", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chrX", "chrY"]
# a pseudo-rule that collects the target files
rule all:
	input:
		tsv = expand(config['RUN']+"/RESULTS/{name}_filtered.tsv", name=NAMES),
		flagstat_pre = expand(config['RUN']+"/METRICS/{sample}_flagstat_pre.txt", sample=SAMPLES)
# change the input to obtain last file of the pipeline

# a pseudo-rule that collects the target files

rule trim_chunks:
        input:
                R1 = expand(config['RUN']+"/TRIMMED/{sample}_R1.duk.fq.gz", sample=SAMPLES),
                R2 = expand(config['RUN']+"/TRIMMED/{sample}_R2.duk.fq.gz", sample=SAMPLES)

rule chunks:
	input:
		flagstat_pre = expand(config['RUN']+"/METRICS/{sample}_flagstat_pre.txt", sample=SAMPLES)

#issues
rule merge_all:
	input:
		merge = expand(config['RUN']+"/BAM/{mysample}_merged.bam", mysample=MYSAMPLES)

rule markdup_all:
	input:
		markdup = expand(config['RUN']+"/BAM/{mysample}_markdup.bam", mysample=MYSAMPLES)

rule freebayes_all:
	input:
		freebayes = expand(config['RUN']+"/VCF/All_{chr}.FB.sorted.vcf.gz", chr=MYCHR)

rule trimming:
	input:
		R1 = RAW_DIR+"/{S}_{I}_{L}_R1_001.fastq.gz",
		R2 = RAW_DIR+"/{S}_{I}_{L}_R2_001.fastq.gz"
	output:
		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz",
		R2 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R2.duk.fq.gz"
	params:
		outdir=config['RUN']+"/TRIMMED/"
	resources:
		mem_mb=15000
	shell:
		"""
		mkdir -p {params.outdir}
		/lustre1/ctgb-usr/local/src/bbmap/bbduk.sh \
		-Xmx14g \
		in={input.R1} in2={input.R2} \
		out={output.R1} out2={output.R2} \
		ref=/lustre1/genomes/Illumina_Adapters/BBDUK/adapters.fa \
		k=23 mink=11 rcomp=t ktrim=f kmask=X qtrim=rl trimq=5 \
		forcetrimleft=2 forcetrimright2=2 overwrite=true
		"""

rule align:
	input:
		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz",
		R2 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R2.duk.fq.gz"
	output:
		bam = config['RUN']+"/BAM/{S}_{I}_{L}.bam",
		index = config['RUN']+"/BAM/{S}_{I}_{L}.bam.bai"
	params:
		rg=r"@RG\tID:{I}\tPL:illumina\tPU:{L}\tSM:{S}\tLB:{I}\tCN:CTGB\tSO:coordinate",
		ref_genome=config['ref_genome'],
		samtools=config['SAMTOOLS'],
		bwa=config['BWA'],
		bamdir=config['RUN']+"/BAM/",
		cmd_cpus="8"
	threads: 9
	resources:
		mem_mb=32000
	shell:
		"""
		mkdir -p {params.bamdir}
		{params.bwa} mem -t {params.cmd_cpus} \
		-R '{params.rg}' \
		{params.ref_genome} {input.R1} {input.R2} \
		| {params.samtools} view -Su - \
		| {params.samtools} sort -T {output.bam}.tmp -@ {params.cmd_cpus} - -o {output.bam}
		{params.samtools} index {output.bam}
		"""

rule flagstat:
	input:
		config['RUN']+"/BAM/{S}_{I}_{L}.bam"
	output:
		config['RUN']+"/METRICS/{S}_{I}_{L}_flagstat_pre.txt"
	params:
		samtools=config['SAMTOOLS'],
		statdir=config['RUN']+"/METRICS/"
	resources:
		mem_mb=2000
	shell:
		"""
		mkdir -p {params.statdir}
		{params.samtools} flagstat {input} > {output}
		"""

rule merge_bams:
	input:
		lane1 = config['RUN']+"/BAM/{S}_{I}_L001.bam",
		lane2 = config['RUN']+"/BAM/{S}_{I}_L002.bam",
		lane3 = config['RUN']+"/BAM/{S}_{I}_L003.bam",
		lane4 = config['RUN']+"/BAM/{S}_{I}_L004.bam"
	output:
		config['RUN']+"/BAM/{S}_{I}_merged.bam"
	threads: 4
	resources:
		mem_mb=32000
	shell:
		"""
		picard.jar MergeSamFiles \
			I={input.lane1} \
			I={input.lane2} \
			I={input.lane3} \
			I={input.lane4} \
			O={output} \
			USE_THREADING=true \
			VALIDATION_STRINGENCY=LENIENT \
			AS=true CREATE_INDEX=true
		"""

#    wrapper:
#        "0.34.0/bio/picard/mergesamfiles"

#rule locatit:
		#java -Xmx48g -Djava.io.tmpdir={params.tempdir} \

rule markdup:
	input:
		#index = RAW_DIR+"/{S}_{I}_{L}_I2_001.fastq.gz",
		bam = config['RUN']+"/BAM/{S}_{I}_merged.bam"
	output:
		marked = config['RUN']+"/BAM/{S}_{I}_markdup.bam",
		metrics = config['RUN']+"/BAM/{S}_{I}_markdup_metrics.txt"
	params:
		tempdir=config['tempdir']
	threads:
		8
	resources:
		mem_mb=48000
	shell:
		"""
		export _JAVA_OPTIONS=-Djava.io.tmpdir={params.tempdir} && \
		java -Xmx48g \
		-jar /usr/local/cluster/bin/picard.jar \
		MarkDuplicates \
		I={input.bam} \
		O={output.marked} \
		M={output.metrics} \
		VALIDATION_STRINGENCY=LENIENT \
		ASSUME_SORTED=coordinate \
		CREATE_INDEX=true
		"""

rule freebayes:
	input:
		all = expand(config['RUN']+"/BAM/{sample}_markdup.bam", sample=MYSAMPLES_BAM)
	output:
		vcfz = config['RUN']+"/VCF/All_{chr}.FB.sorted.vcf.gz",
		tbi = config['RUN']+"/VCF/All_{chr}.FB.sorted.vcf.gz.tbi"
	params:
		ref_genome_fa=config['ref_genome_fa'],
		vcfdir=config['RUN']+"/VCF/"
	resources:
		mem_mb=48000
	shell:
		"""
		#check freebayes version
		mkdir -p {params.vcfdir}
		#freebayes -f {params.ref_genome_fa}
		condactivate freebayes;
		freebayes_1.1.0-3 -f {params.ref_genome_fa} \
		-F 0.2 -C 2 -q 20 -m 1 \
		-t chr/hg38_{wildcards.chr}.bed \
		--genotype-qualities --min-repeat-entropy 1 \
		--report-genotype-likelihood-max \
		-b {input.all} | vcfsort \
		| vcfuniq | vcfsort | vcffixup - | bgzip -fc > {output.vcfz}
		tabix -f {output.vcfz}
		"""

#not used after freebayes_1.0.5 version
rule fix_dictionary:
	input:
		vcfz = config['RUN']+"/VCF/All_{chr}.FB.sorted.vcf.gz"
	output:
		fix = config['RUN']+"/VCF/All_{chr}.FB.sorted.fix.vcf.gz",
		tbi = config['RUN']+"/VCF/All_{chr}.FB.sorted.fix.vcf.gz.tbi"
	params:
		ref_genome_fa=config['ref_genome_fa'],
		ref_genome_dict=config['ref_genome_dict'],
		tempdir=config['tempdir']
	resources:
		mem_mb=48000
	shell:
		"""
		export _JAVA_OPTIONS=-Djava.io.tmpdir={params.tempdir} && \
		java -Xmx48g \
		-jar /usr/local/cluster/bin/picard.jar \
		UpdateVcfSequenceDictionary \
		I={input.vcfz} \
		O={output.fix} \
		SD={params.ref_genome_dict} \
		TMP_DIR={params.tempdir} \
		CREATE_INDEX=true \
		R={params.ref_genome_fa}
		"""

rule merge_vcf:
	input:
		expand(config['RUN']+"/VCF/All_{chr}.FB.sorted.vcf.gz", chr=MYCHR)
	output:
		merged = config['RUN']+"/VCF/All_merged.vcf.gz",
		tbi = config['RUN']+"/VCF/All_merged.vcf.gz.tbi"
	params:
		files = lambda wildcards, input: " I=".join(input),
		tempdir=config['tempdir'],
		ref_genome_fa=config['ref_genome_fa']
	shell:
		"""
		export _JAVA_OPTIONS=-Djava.io.tmpdir={params.tempdir} && \
		java -Xmx48g \
		-jar /usr/local/cluster/bin/picard.jar \
		GatherVcfs \
		I={params.files} \
		O={output.merged} \
		TMP_DIR={params.tempdir}
		tabix -f {output.merged}
		"""

rule vcf_filtering:
	input:
		vcfz = config['RUN']+"/VCF/All_merged.vcf.gz"
	output:
		vcfz = config['RUN']+"/VCF/All_merged.QUAL.vcf.gz",
		tbi = config['RUN']+"/VCF/All_merged.QUAL.vcf.gz.tbi"
	params:
		ref_genome_fa=config['ref_genome_fa']
	resources:
		mem_mb=4000
	conda:
		"conda-base.yaml"
	shell:
		"""
		zcat {input.vcfz}  \
		| snpSift filter  "( QUAL > 0 ) & ( QUAL / AO > 1 ) & ( SAF > 0 & SAR > 0 ) & ( RPR > 1 & RPL > 1 ) & ( MQM > 10 | MQMR > 10 )" \
		| vcfentropy -w 10 -f {params.ref_genome_fa} \
		| snpSift  filter -n "(EntropyLeft < 0.1) || (EntropyRight < 0.1) " \
		| bcftools filter -s "test" -g 3 -G 1 \
		| vcfsort | vcffixup - | bgzip -c > {output.vcfz}
		tabix -f {output.vcfz}
		"""

rule vcf_annotate:
	input:
		vcfz = config['RUN']+"/VCF/All_merged.QUAL.vcf.gz"
	output:
		vcfz = config['RUN']+"/VCF/All_merged_annotate.vcf.gz",
		idx_sample = config['RUN']+"/VCF/All_merged_annotate.vcf.gz.tbi"
	params:
		dbsnp=config['dbsnp'],
		cosmic_dir=config['cosmic_dir'],
		dbnsfp=config['dbnsfp'],
		dbnsfp_ann=config['dbnsfp_ann'],
		snpEff=config['snpEff'],
		ucsc_version=config['ucsc_version'],
		ncbi_version=config['ncbi_version'],
		ref_genome_fa=config['ref_genome_fa']
	resources:
		mem_mb=48000
	threads: 10
	conda:
		"conda-base.yaml"
	shell:
		"""
		vt normalize -r {params.ref_genome_fa} {input.vcfz} \
		| snpSift annotate {params.dbsnp} \
		| snpSift annotate {params.cosmic_dir}/CosmicCodingMuts.vcf.gz \
		| snpSift annotate {params.cosmic_dir}/CosmicNonCodingVariants.vcf.gz \
		| snpSift dbnsfp -db {params.dbnsfp} -g {params.ucsc_version} -f "{params.dbnsfp_ann}" -collapse - \
		| snpEff -noStats -v -lof {params.ncbi_version} -dataDir {params.snpEff} \
		| vcfsort \
		| bgzip -f -c \
		> {output.vcfz}
		tabix -f {output.vcfz}
		"""

#not used in wgs
rule vcf_suspicious:
	input:
		vcfz = config['RUN']+"/VCF/All_{chr}.FB.QUAL.vcf.gz"
	output:
		vcfz = config['RUN']+"/VCF/All_{chr}.FB.QUAL.S.vcf.gz",
		tbi = config['RUN']+"/VCF/All_{chr}.FB.QUAL.S.vcf.gz.tbi"
	resources:
		mem_mb=4000
	shell:
		"""
		bcftools filter -sSuspicious -g3 -G10 -e ' \
		AB < 1 \
		&& QUAL <= 1 \
		&& AVG(DP) > 500 || AVG(DP) < 20 \
		&& QUAL <= 20 ||  MAX(FMT/AO[*]) < 3 \
		' {input.vcfz} | \
		pbgzip -fc > {output.vcfz}
		tabix {output.vcfz}
		"""

rule vcf_sample:
	input:
		vcfz = config['RUN']+"/VCF/All.FB.QUAL.S.vcf.gz"
	output:
		vcf_sample = config['RUN']+"/VCF/{name}_sample.vcf.gz",
		tbi_sample = config['RUN']+"/VCF/{name}_sample.vcf.gz.tbi"
	params:
		ref_genome_fa=config['ref_genome_fa']
	resources:
		mem_mb=4000
	conda:
		"conda-base.yaml"
	shell:
		"""
 		bcftools view {input.vcfz} | \
		vcfkeepsamples - {wildcards.name} | snpSift filter "(GEN[ANY].AO >= 2)"| \
		vcfsort | vcffixup - | vt normalize -r {params.ref_genome_fa} - | \
		bgzip -fc > {output.vcf_sample}
		tabix -fp vcf {output.vcf_sample}
		"""

rule vcf_annotate_sample:
	input:
		vcf_sample = config['RUN']+"/VCF/{name}_sample.vcf.gz"
	output:
		vcf_sample = config['RUN']+"/VCF/{name}_sample_annotate.vcf.gz",
		idx_sample = config['RUN']+"/VCF/{name}_sample_annotate.vcf.gz.tbi"
	params:
		dbsnp=config['dbsnp'],
		cosmic_dir=config['cosmic_dir'],
		dbnsfp=config['dbnsfp'],
		snpEff=config['snpEff'],
		ucsc_version=config['ucsc_version'],
		ncbi_version=config['ncbi_version']
	resources:
		mem_mb=8000
	conda:
		"conda-base.yaml"
	shell:
		"""
		#vt normalize ...
		snpSift annotate {params.dbsnp} {input.vcf_sample} | \
		snpSift annotate {params.cosmic_dir}/CosmicCodingMuts.vcf.gz | \
		snpSift annotate {params.cosmic_dir}/CosmicNonCodingVariants.vcf.gz | \
		snpSift annotate {params.clinvar} | \
		snpSift dbnsfp -db {params.dbnsfp} -g {params.ucsc_version} -collapse - | \
		snpEff -noStats -v -lof {params.ncbi_version} -dataDir {params.snpEff} \
		| vcfsort \
		| bgzip -f -c \
		> {output.vcf_sample}
		tabix -f {output.vcf_sample}
		"""

rule tsv_filter:
	input:
		vcf_sample = config['RUN']+"/VCF/{name}_sample_annotate.vcf.gz"
	output:
		tsv = config['RUN']+"/RESULTS/{name}_filtered.tsv"
	params:
		tsvdir = config['RUN']+"/RESULTS/"
	resources:
		mem_mb=2000
	conda:
		"conda-base.yaml"
	shell:
		"""
		mkdir -p {params.tsvdir}
		zcat {input} \
		| snpSift filter "\
		   (EFF[0].IMPACT == 'HIGH') \
		|| (EFF[0].IMPACT == 'MODERATE') \
		|| (EFF[0].IMPACT == 'MODIFIER') \
		|| (EFF[0].EFFECT =~ 'splice') \
		" \
	 	| snpSift filter "(SNP=~'false') || ((SNP)&(CLNSIG[0] > 0))" \
		| snpSift extractfields  -s ";" -e "." - "ID" "RS[0]" "EFF[0].GENE" "EFF[0].GENEID" "EFF[0].FEATUREID" \
		"ANN[0].RANK" "EFF[0].ALLELE" "EFF[0].HGVS_C" "EFF[0].HGVS_P"  "ANN[0].CDNA_POS" "ANN[0].AA_POS" \
		"FILTER" "CHROM" "POS" "STRAND[0]" "REF" "ALT[0]" "RV" "EntropyLeft" "EntropyRight" \
		"EFF[*].AA" "EFF[*].RANK" "EFF[0].IMPACT" "EFF[0].EFFECT" \
		"CLNSIG[0]" "CLNREVSTAT[0]" "CLNDBN[0]" "CLNDSDBID[0]" "CLNHGVS[0]" \
		"PMC" "PM" "G5" "G5A" "SNP" "COMMON" "KGPhase1" "KGPhase3" \
		"CAF" "VC" "MUT" "AF" "GEN[0].GT[0]" "GEN[0].DP[0]" "GEN[0].RO[0]" "GEN[0].AO" "GEN[0].QA" \
		> {output.tsv}
		"""

rule hello_world:
  input: "world.txt"
  output: "hello_world.txt"
  conda:
    "conda-base.yaml"
  shell:
    " echo 'Hello' |  cat - {input} > {output} "


#snippet to concat hsmetrics with correct sample names...header should still be added
#tail -n3 199BSC-271114_sorted.hsmetrics | head -n1 | awk -v sample=$(basename 199BSC-271114_sorted.hsmetrics | sed 's/.hsmetrics$//') '{OFS="\t"}{print $0,sample,sample,sample}' | cut -f -43,46-

#tsv
#zcat All_merged_extracted_dbNSFP_samples.vcf.gz | bawk '{if ($1=="#CHROM") gsub("-","_",$0); print $0}' | snpSift extractFields -s ';' -e '.' - CHROM POS ID REF ALT QUAL ANN[*].IMPACT ANN[*].GENE ANN[*].EFFECT ANN[*].FEATUREID dbNSFP_ExAC_Adj_AF $(zcat All_merged_extracted_dbNSFP_samples.vcf.gz | grep -v ^## | grep ^# -m1 | explain_first_row | tail -n +10 | cut -f 2 | bawk '{print "GEN["$1"].GT","GEN["$1"].AO","GEN["$1"].DP"}' | tr '\n' '\t') | bgzip -c > extracted.tsv.gz
